const {Router} = require('express');
const Course = require('../models/course.js');
const router = Router();

router.get('/', (req, res) => {
    res.render("add",{
        title:"Add",
        isAdd:true
    });
});

router.post("/", async (req, res) => {
   // const course = new Course(req.body.title, req.body.price, req.body.img);
   // await course.save()
   // res.redirect("/courses")

    const course = new Course({
        title: req.body.title,
        price: req.body.price,
        img: req.body.img,
        userId: req.user,
    });

    try {
        await course.save();
        res.redirect("/courses");
    }
    catch (err){
        console.log(err);
    }


});

module.exports = router;