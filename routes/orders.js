const {Router}  = require('express');
const Order     = require('../models/order');
const router    = Router();

router.get('/',  async (req, res)=>{
   try {
       const orders = await Order.find({'user.userId':req.user._id}).populate('user.userId');
       const ordersArray = orders.map(x=>{
           return {
               ...x._doc,
               price:x.courses.reduce((total, c)=>{
                    return total += c.count * c.course.price
               },0)
           }
       })
       res.render('orders',{
           title:'Orders',
           isOrder:true,
          orders:ordersArray
       });
   }
   catch (e){
       console.log(e)
   }
});

router.post('/', async (req, res) =>{
    try{
        const user = await req.user.populate('cart.items.courseId').execPopulate();
        const courses = user.cart.items.map(item =>({
            count:item.count,
            course:{...item.courseId._doc}
        }));
        const order = new Order({
            courses:courses,
            user:{
                name:req.user.name,
                userId:req.user._id
            }
        });
        await order.save();
        await req.user.clearCart();
        res.redirect('/courses');
    }
    catch (e){
        console.log(e)
    }
});

module.exports = router;