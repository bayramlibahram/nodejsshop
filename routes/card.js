const {Router}  = require('express');
const router    = Router();
const Course    = require('../models/course.js');

function mapCartItems(cart){
    return cart.items.map(x=>({
        ...x.courseId._doc,
        id:x.courseId.id,
        count: x.count
    }));
}

function computePrice(course){
    return course.reduce((totalPrice,course)=>{
        return totalPrice += course.price * course.count;
    },0)
}

router.get('/', async (req,res) =>{
    const user      = await req.user.populate('cart.items.courseId').execPopulate();
    //console.log(user.cart.items);

    const courses   = mapCartItems(user.cart);

    res.render('card',{
        title:'Carts',
        isCard:true,
        courses:courses,
        price:computePrice(courses),
    })
    //res.json({test:true});
});

router.post('/add', async (req,res) =>{
    const course = await Course.findById(req.body.id);
    await req.user.AddToCart(course);
    res.redirect("/card");
});

router.delete("/remove/:id", async (req, res) =>{
    await req.user.removeFromCart(req.params.id);
    const user = await req.user.populate('cart.items.courseId').execPopulate();
    const courses = mapCartItems(user.cart);
    const cart = {
        courses,
        price:computePrice(courses)
    }
    res.status(200).json(cart);
});

module.exports = router;