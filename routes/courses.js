const {Router}  = require('express');
const Course    = require("../models/course.js");
const router    = Router();

router.get('/', async (req, res) => {
    const courses = await Course.find().populate('userId',"name email").lean();
    console.log(courses);
    res.render("courses",{
        title:"Courses",
        isCourses:true,
        courses
    });
});

router.get('/:id', async (req,res)=>{
    const course = await Course.findById(req.params.id).lean();
    res.render('course',{
        layout:"empty",
        title:course.title,
        course
    });
});

router.get('/:id/edit', async (req,res)=>{
   if(!req.query.allow){
       return res.redirect("/courses");
   }
    const course = await Course.findById(req.params.id).lean();
    res.render("course-edit",{
       title:`Edit course : ${course.title}`,
       course
    });
});

router.post('/edit', async (req,res) =>{
    const {id} = req.body;
    delete req.body.id;
    await Course.findOneAndUpdate(id, req.body);
    res.status(200).redirect('/courses');

});

router.post("/remove", async (req,res) =>{
  try {
      await Course.deleteOne({
          _id:req.body.id,
      });
      res.status(200).redirect('/courses');
  }
  catch (err){
      console.log(err);
  }
});

module.exports = router;