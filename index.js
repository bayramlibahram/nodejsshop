const compression       = require('compression');
const express           = require('express');
const app               = express();
const mongoose          = require('mongoose');
const path              = require('path');
const exphbs            = require('express-handlebars');
const homeRouters       = require('./routes/home.js');
const coursesRouters    = require('./routes/courses.js');
const addRouters        = require('./routes/add.js');
const ordersRouters     = require('./routes/orders.js');
const cardRouters       = require('./routes/card.js');
const User              = require('./models/user.js');

const hbs               = exphbs.create({
    defaultLayout: "main",
    extname:"hbs",
    runtimeOptions: {
        allowProtoPropertiesByDefault: true,
        allowProtoMethodsByDefault: true
    }
});

const PORT              = process.env.PORT  || 80;

app.engine("hbs",hbs.engine);
app.set("view engine","hbs");
app.set("views","views");

app.use(compression());

app.disable('etag');
app.use(express.static(path.join(__dirname, "public")));
app.use(express.urlencoded({extended: true}));
app.use(async  (req, res, next) =>{
    try{
        const user = await User.findById('60d32e1b2eddeba15f75cdfb');
        req.user = user;
        next();
    }
    catch (e){
        console.log(e);
    }
});
app.use("/",homeRouters);
app.use("/courses",coursesRouters);
app.use("/add",addRouters);
app.use("/card",cardRouters);
app.use("/orders",ordersRouters);


async function start(){

    const DB_LOGIN          = "bahram94";
    const DB_PASSWORD       = "umFjIvefGM47lP8b";
    const DB_URL            = `mongodb+srv://${DB_LOGIN}:${DB_PASSWORD}@cluster0.3mpbf.mongodb.net/shop`

    try {
        await mongoose.connect(DB_URL,{
            useNewUrlParser:true,
            useUnifiedTopology:true,
            useFindAndModify:false
        }).then( ()=>{
            console.log("DB connected successfully !");
        });
        const candidate = await User.findOne();
        if (!candidate){
            const user = new User({
                name:'Bahram',
                email:'bahrambayramli@hotmail.com',
                cart: {items:[]}
            });

            await user.save();
        }

        app.listen(PORT, ()=>{
            console.log(`Server has been started on port ${PORT}`);
        });
    }
    catch (e){
        console.log(e);
    }


}

start();

