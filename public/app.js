const toCurrency  = price =>{
   return new Intl.NumberFormat('az-AZ',{
        currency:'azn',
        style:'currency'
    }).format(price)
}

const toDate = date =>{
    return new Intl.DateTimeFormat('az-AZ',{
        day:'2-digit',
        month:'long',
        year:'numeric',
        hour:'2-digit',
        minute:'2-digit',
        second:'2-digit'
    }).format(new Date(date));
}


document.querySelectorAll('.price').forEach(node=>{
    node.textContent = toCurrency(node.textContent);
});

document.querySelectorAll('.card-date').forEach(node=>{
    node.textContent = toDate(node.textContent);
});

const $card = document.querySelector("#card");

if($card){
    $card.addEventListener("click", event =>{
        //console.log(event);
        if(event.target.classList.contains("js-remove")){
            const id = event.target.dataset.id;
           fetch(`/card/remove/${id}`,{
               method:'delete',
           })
           .then(res => res.json())
           .then(card => {
                if(card.courses.length){
                    const tableTemplate = card.courses.map(x=>{
                        return `
                         <tr>
                          <td class='text-center'>${x.title}</td>
                          <td class="text-center">${x.count}</td>
                          <td class="text-center">${x.price}</td>
                          <td class="text-center">
                            <button class="btn btn-danger js-remove" data-id="${x.id}">Delete</button>
                          </td>
                        </tr>
                        `
                    }).join("");
                   $card.querySelector('tbody').innerHTML = tableTemplate;
                   console.log(card.price);
                   console.log($card.querySelector('.price'));
                   $card.querySelector('.price').textContent = toCurrency(card.price);
                }
                else {
                    $card.innerText = "Card is empty"
                }
           })

        }
    });
}
