const fs        = require('fs');
const path      = require('path');

class Card {

   static async add(course){
       const card   = await Card.fetch();
       //console.log("card:",card);
       const idx    = card.courses.findIndex(x => x.id === course.id);
       console.log("Index:",idx); //What is that ?
       const candidate = card.courses[idx];
       console.log("Candidate:",candidate); // ???

       if(candidate){
           // console.log("Candidate",candidate)
           //Course is already exist
           candidate.count++;
           card.courses[idx] = candidate;

       }
       else {
           //need to add course to card
           course.count = 1;
           card.courses.push(course);
       }

       card.price += +course.price;

       return new Promise((resolve,reject)=>{
            fs.writeFile(
                path.join(__dirname, "..","data","card.json"),
                JSON.stringify(card),
                err => {
                    if (err) reject(err);
                    else  resolve();
                }
            );
       });
    }

    static async fetch(){
        return new Promise((resolve, reject)=>{
           fs.readFile(
               path.join(__dirname, "..","data","card.json"),
               "utf-8",(err, data) => {
               if (err) reject(err);
               else  resolve(JSON.parse(data));
           });
        });
    }

    static async remove(id){

       const card   = await Card.fetch(); //get all courses from cards
       const idx    = card.courses.findIndex(x => x.id === id); //find course index from card.course
       const course = card.courses[idx]; // the course in the card

       if(course.count === 1 ){ // if course.count equal to 1
           //delete the course from card list
           card.courses  = card.courses.filter(x => x.id !== id); //nothing is changed
       }
       else{
           card.courses[idx].count --;
       }
       card.price -= course.price;

        return new Promise((resolve,reject)=>{
            fs.writeFile(
                path.join(__dirname, "..","data","card.json"),
                JSON.stringify(card),
                err => {
                    if (err) reject(err);
                    else  resolve(card);
                }
            );
        });
    }
}

module.exports = Card;