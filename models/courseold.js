//ES6
const { v4: uuid }  = require('uuid');
const fs            = require('fs');
const path          = require('path');
class Courseold {

    constructor(title, price, img){
        this.title  = title;
        this.price  = price;
        this.img    = img;
        this.id     = uuid();
    }

    toJson(){
        return {
           title    :this.title ,
           price    :this.price ,
           img      : this.img  ,
            id      : this.id
        };
    }

    async save() {
        const courses = await Courseold.getAll();
        courses.push(this.toJson());
        return new Promise((resolve, reject)=>{
            fs.writeFile(
              path.join(__dirname, "..","data","courses.json"),
              JSON.stringify(courses),
                (err)=>{
                  if(err) reject(err);
                  else  resolve();
                }
            );
        });
    }

    static async update(course){
        const courses   = await Courseold.getAll();
        const index     = courses.findIndex(x=> x.id === course.id);
        courses[index]  = course;
        return new Promise((resolve, reject)=>{
            fs.writeFile(
                path.join(__dirname, "..","data","courses.json"),
                JSON.stringify(courses),
                (err)=>{
                    if(err) reject(err);
                    else  resolve();
                }
            );
        });
    }

    static getAll(){
        return new Promise((resolve, reject)=>{
            fs.readFile(
                path.join(__dirname,"..","data","courses.json"),
                "utf-8",
                (err,content)=>{
                    if(err) reject(err);
                    else resolve(JSON.parse(content));
                }
            );
        });
    }

    static async getById(id){
        const courses = await Courseold.getAll();
        return courses.find(x=> x.id === id);
    }
}

module.exports = Courseold;