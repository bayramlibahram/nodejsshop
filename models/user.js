const {Schema, model} = require('mongoose');

const userSchema = new Schema({
   name:{
       type:String,
       required: true
   } ,
    email:{
        type:String,
        required:true
   } ,
    cart:{

       items:[
           {
               count:{
                   type:Number,
                   required:true,
                   default:1
               },
               courseId:{
                   type:Schema.Types.ObjectId,
                   required:true,
                   ref:'Course'
               }
           }
       ]
    }
});

userSchema.methods.AddToCart = function(course){

    const clonedItems = [...this.cart.items];

    const idx = clonedItems.findIndex(x => {
        return x.courseId.toString() === course._id.toString();
    });
    console.log(idx);
    if(idx >=0){
        clonedItems[idx].count = clonedItems[idx].count + 1;

    }
    else {
        clonedItems.push({
           courseId:course._id,
           count:1
        });
    }

    this.cart = {items:clonedItems};
    this.save();
}

userSchema.methods.removeFromCart = function(id){
    let items = [...this.cart.items];
    const index = items.findIndex(x => x.courseId.toString() === id.toString());
    if(items[index].count === 1){
        items = items.filter(x => x.courseId.toString() !== id.toString());
    }
    else {
        items[index].count --;
    }
    this.cart = {items};
    this.save();
}

userSchema.methods.clearCart = function(){
    this.cart = {items:[]};
    return this.save();
}

module.exports = model("User", userSchema);